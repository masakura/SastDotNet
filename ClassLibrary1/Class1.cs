﻿using System.Diagnostics;

namespace ClassLibrary1;

public class Class1
{
    public static void Invoke(string command)
    {
        Process.Start(command);
    }
}